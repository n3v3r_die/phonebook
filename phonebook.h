#ifndef PHONEBOOK_H
#define PHONEBOOK_H

#include <QtWidgets>
#include <QApplication>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QtSql>
#include <QErrorMessage>
#include "libs/phonebookmodel.h"
#include "login.h"

class PhoneBook : public QWidget
{
    Q_OBJECT

public:
    PhoneBook(QWidget *parent = nullptr);
    ~PhoneBook();
    QSqlDatabase db;

private:
    QMenu * menu;
    Login * lgnForm;
    QAction * actConnect;
    PhoneBookModel * model;
    QTableView * tableView;
    QComboBox * comboOtdSearch;
    QLineEdit * lineEditSearch;
    QSystemTrayIcon * ptrayIcon;
    void login();
    void setupMenu();
    void setupTrayIcon();
    void setupForm();
    void setupModel();
    void setupComboBox();

private slots:
    void terminateApp();
    void setupConnect();
    void on_lineEditSearch_returnPressed();
    void trayActivate(QSystemTrayIcon::ActivationReason reason);
    void onSuccessLogin();
};

#endif // PHONEBOOK_H
