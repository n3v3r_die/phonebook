#include "phonebook.h"

PhoneBook::PhoneBook(QWidget *parent)
    : QWidget(parent)
{
    db = QSqlDatabase::addDatabase("QOCI");
    lgnForm = new Login(&db, this);
    setupMenu();
    setupTrayIcon();
    setupForm();
    connect(lgnForm, SIGNAL(loginSuccess()), this, SLOT(onSuccessLogin()));
}

PhoneBook::~PhoneBook()
{

}

void PhoneBook::setupMenu()
{
    menu = new QMenu(this);
    actConnect = menu->addAction(QIcon("://login"),"&Подключиться");
    QAction * actQuit = menu->addAction(QIcon("://close"),"&Выход");
    connect(actConnect, SIGNAL(triggered()), this, SLOT(setupConnect()));
    connect(actQuit, SIGNAL(triggered()), this, SLOT(terminateApp()));
}

void PhoneBook::setupTrayIcon()
{
    ptrayIcon = new QSystemTrayIcon(QIcon("://phonebook"), this);
    ptrayIcon->setToolTip("Телефонный справочник");
    ptrayIcon->setContextMenu(menu);
    ptrayIcon->show();
    connect(ptrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(trayActivate(QSystemTrayIcon::ActivationReason)));
}

void PhoneBook::setupForm()
{
    setWindowTitle("Телефоная книга");
    setWindowIcon(QIcon("://phonebook"));
    this->setMinimumSize(QSize(750, 450));
    tableView = new QTableView(this);
    tableView->sortByColumn(0, Qt::AscendingOrder);
    tableView->setSortingEnabled(true);
    comboOtdSearch = new QComboBox(this);
    lineEditSearch = new QLineEdit(this);
    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    QHBoxLayout * searchLayout = new QHBoxLayout;
    QAction *act = lineEditSearch->addAction(QIcon("://search"),
                                             QLineEdit::LeadingPosition);
    QLabel * labelLine = new QLabel(this);
    labelLine->setObjectName("labelLine");
    labelLine->setMaximumSize(QSize(4096, 2));
    labelLine->setStyleSheet("#labelLine{"
                             "border-top: 2px solid rgb(65, 205, 82);"
                             "}");
    mainLayout->setSpacing(5);
    mainLayout->addWidget(tableView);
    mainLayout->addLayout(searchLayout);
    mainLayout->addWidget(labelLine);
    searchLayout->addWidget(lineEditSearch);
    searchLayout->addWidget(comboOtdSearch);
    lineEditSearch->setToolTip("Поиск по фамилии");
    comboOtdSearch->setToolTip("Фильтр по отделу");
    setTabOrder(lineEditSearch, comboOtdSearch);
    setTabOrder(comboOtdSearch, tableView);
    connect(act, SIGNAL(triggered()), this, SLOT(on_lineEditSearch_returnPressed()));
    connect(lineEditSearch, SIGNAL(returnPressed()), this, SLOT(on_lineEditSearch_returnPressed()));
}

void PhoneBook::login()
{
    lgnForm->show();
    lgnForm->activateWindow();
}

void PhoneBook::terminateApp()
{
    qApp->exit();
}

void PhoneBook::setupConnect()
{
    login();
}

void PhoneBook::on_lineEditSearch_returnPressed()
{
    QVariantList var = {lineEditSearch->text().trimmed(),
                        comboOtdSearch->currentText().trimmed()};
    model->bindVariables(var);
    if (model->lastError().isValid())
        (new QErrorMessage)->showMessage(model->lastError().text());
}

void PhoneBook::setupModel()
{
    model = new PhoneBookModel(this);
    if (model->lastError().isValid())
        (new QErrorMessage)->showMessage(model->lastError().text());
    model->setHeaderData(0, Qt::Horizontal, "Фамилия");
    model->setHeaderData(1, Qt::Horizontal, "Имя");
    model->setHeaderData(2, Qt::Horizontal, "Фамилия");
    model->setHeaderData(3, Qt::Horizontal, "Телефон");
    model->setHeaderData(4, Qt::Horizontal, "Внутр. email");
    model->setHeaderData(5, Qt::Horizontal, "Внеш. email");
    model->setHeaderData(6, Qt::Horizontal, "КОТД");
    model->setHeaderData(7, Qt::Horizontal, "Должность");
    model->setHeaderData(8, Qt::Horizontal, "Подразделение");
    tableView->setModel(model);
    tableView->resizeColumnsToContents();
    tableView->setColumnWidth(3, 60);
}

void PhoneBook::setupComboBox()
{
    QSqlQueryModel * filterModel= new QSqlQueryModel(this);
    filterModel->setQuery("select 'Все отделы' kr_name from dual "
                          "union "
                          "select distinct (kr_name) from kvs.vuspr_email_new ");
    if (filterModel->lastError().isValid()){
        (new QErrorMessage)->showMessage(filterModel->lastError().text());
        return;
    }
    comboOtdSearch->setEditable(true);
    comboOtdSearch->setModel(filterModel);
    comboOtdSearch->setCurrentIndex(comboOtdSearch->findText("Все отделы"));
    connect(comboOtdSearch, SIGNAL(currentIndexChanged(int)), this, SLOT(on_lineEditSearch_returnPressed()));
}

void PhoneBook::trayActivate(QSystemTrayIcon::ActivationReason reason)
{
    if (reason == QSystemTrayIcon::DoubleClick) {
        if (!db.isOpen())
            login();
        else{
            lineEditSearch->setFocus();
            show();
            activateWindow();
        }
    }
}

void PhoneBook::onSuccessLogin()
{
    if (db.isOpen()){
        actConnect->setEnabled(false);
        actConnect->setText("Подключено");
        lineEditSearch->setFocus();
        setupModel();
        setupComboBox();
        show();
    }
}
