#include "login.h"
#include "ui_login.h"
#include <QDir>
#include <QSettings>
#include <QSqlError>
#include <QErrorMessage>
#include "libs/simplecrypt.h"

Login::Login(QSqlDatabase *db, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);
    this->db = db;
    key = 0x0c2ad13ffcb130de;
    setWindowTitle("Вход");
    loadSettings();
}

Login::~Login()
{
    delete ui;
}

void Login::on_loginButton_clicked()
{
    db->setDatabaseName(ui->lineEditDB->text());
    db->setUserName(ui->lineEditUser->text());
    db->setPassword(ui->lineEditPassword->text());
    if (!db->open())
        (new QErrorMessage())->showMessage(
            db->lastError().text());
    else {
        saveSettings();
        emit loginSuccess();
        close();
    }
}

void Login::saveSettings()
{

    QString path = QDir::currentPath();
    QSettings fConfig(path +"/config.ini", QSettings::IniFormat);
    fConfig.setValue("lastLogin", db->userName());
    fConfig.setValue("lastDataBase", db->databaseName());
    if (ui->checkBoxPass->isChecked()){
        SimpleCrypt crypto(key);
        QString hashPass = crypto.encryptToString(ui->lineEditPassword->text());
        fConfig.setValue("hash", hashPass);
    }else {
        fConfig.remove("hash");
    }
}

void Login::loadSettings()
{
    QString path = QDir::currentPath();
    QSettings fConfig(path +"/config.ini", QSettings::IniFormat);

    ui->lineEditUser->setText(fConfig.value("lastLogin", "UserName").toString());
    ui->lineEditDB->setText(fConfig.value("lastDataBase", "GEMINI").toString());
    QString hashPass = fConfig.value("hash", "").toString();
    if (!hashPass.isEmpty()){
        SimpleCrypt crypt(key);
        ui->checkBoxPass->setChecked(true);
        ui->lineEditPassword->setText(crypt.decryptToString(hashPass));
    }
}
