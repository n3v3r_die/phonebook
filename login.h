#ifndef LOGIN_H
#define LOGIN_H

#include <QDialog>
#include <QSqlDatabase>

namespace Ui {
class Login;
}

class Login : public QDialog
{
    Q_OBJECT

public:
    explicit Login(QSqlDatabase *db, QWidget *parent = nullptr);
    ~Login();

signals:
    void loginSuccess();

private slots:
    void on_loginButton_clicked();


private:
    Ui::Login *ui;
    QSqlDatabase *db;
    quint64 key;
    void saveSettings();
    void loadSettings();
};

#endif // LOGIN_H
