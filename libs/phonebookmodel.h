#ifndef PHONEBOOKMODEL_H
#define PHONEBOOKMODEL_H

#include <QDebug>
#include <QMap>
#include <QtSql>

class PhoneBookModel : public QSqlQueryModel
{
    Q_OBJECT
public:
    explicit PhoneBookModel(QObject * parent = nullptr):
        QSqlQueryModel(parent)
    {
        QString query = "select initcap(surname) surname, initcap(name) name, initcap(patronymic) patr, "
                       " phone, in_email_addr, out_email_addr, kotd, nomen19, kr_name  "
                       " from kvs.vuspr_email_new "
                       " where surname like upper(:pSurname)|| '%' "
                       " and (phone is not null or in_email_addr is not null or out_email_addr is not null)"
                       " and name <> 'СЛУЖЕБНЫЙ' "
                       " and upper(kr_name) = case nvl(:pKr_name, 'Все отделы') when 'Все отделы' then upper(kr_name) else upper(:pKr_name) end"
                       " order by 1";
        QSqlQuery sql;
        sql.prepare(query);
        sql.bindValue(":pSurname", "");
        sql.bindValue(":pKr_name", "");
        if (!sql.exec()){
            setLastError(sql.lastError());
            return;
        }
        setQuery(sql);
    }
    void sort(int column, Qt::SortOrder order) override
    {
        QString selectStmt = query().lastQuery();
        if (selectStmt.contains("order by"))
            selectStmt.remove(selectStmt.indexOf("order by"), selectStmt.length());
        QString orderByClause = QString(" order by %1 %2")
                .arg(QString::number(column+1))
                .arg((order == Qt::DescendingOrder) ? "desc" : "asc");
        beginResetModel();
        QSqlQuery sql;
        sql.prepare(selectStmt + orderByClause);
        QMap <QString, QVariant> variables = query().boundValues();
        sql.bindValue(":pSurname", variables[":pSurname"]);
        sql.bindValue(":pKr_name", variables[":pKr_name"]);
        if (!sql.exec()){
            setLastError(sql.lastError());
            endResetModel();
            return;
        }
        setQuery(sql);
        endResetModel();
    }
    void bindVariables(QVariantList var){
        QString sql = query().lastQuery();
        beginResetModel();
        QSqlQuery qu;
        qu.prepare(sql);
        qu.bindValue(":pSurname", var.at(0));
        qu.bindValue(":pKr_name", var.at(1));
        if (!qu.exec()){
            setLastError(qu.lastError());
            endResetModel();
            return;
        }
        setQuery(qu);
        endResetModel();
    }
};


#endif // PHONEBOOKMODEL_H
